﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    public ObstacleMovementTypes MovementType = ObstacleMovementTypes.Vertical;
    public float speed = 5;


    void Update()
    {
        switch(MovementType)
        {
            case ObstacleMovementTypes.Vertical:
            {
                transform.Translate(0, speed * Time.deltaTime, 0);
                break;
            }
            case ObstacleMovementTypes.Horizontal:
            {
                transform.Translate(speed * Time.deltaTime, 0, 0);
                break;
            }
            case ObstacleMovementTypes.Random:
            {
                // TODO: Random obstacle movement
                break;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Collider"))
        {
            speed = speed * -1;
        }
    }

    public enum ObstacleMovementTypes
    {
        Vertical,
        Horizontal,
        Random
    }
}
