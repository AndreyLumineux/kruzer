﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    public GameObject FinishMenu;
    public GameObject DeathMenu;
    public GameControl gameControl;
    public PlayerFillBar fillBar;
    public GameObject smallSpawnEffect;
    public GameObject spawnEffect;

    [SerializeField]
    private int healthPoints = 1;
    public int scor = 0;
    

    private Movement movement;
    private BoxCollider2D boxCollider;

    public static PlayerInteract Instance { get; private set; }


    private void Awake()
    {
        if (Instance && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;


        movement = GetComponent<Movement>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void OnEnable()
    {
        movement.enabled = false;
        boxCollider.enabled = false;
    }

    public void Update()
    {
        if (scor >= gameControl.minScore)
        {
			gameControl.FinishPickup.SetActive(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D altcineva)
	{
		if (altcineva.CompareTag("Pickup"))
		{
			altcineva.gameObject.SetActive(false);
			scor = scor + 1;
			
			altcineva.GetComponent<PickupInfo>()?.InvokeEffect();

			fillBar.SetFillBar((float)scor / gameControl.minScore * 0.9f);
		}
		else if (altcineva.CompareTag("Obstacle"))
        {
	        Movement.Instance.Reset();
	        Movement.Instance.MoveTo(altcineva.transform.position, false);
	        Movement.Instance.enabled = false;

	        altcineva.GetComponent<PickupInfo>()?.InvokeEffect();

	        altcineva.gameObject.SetActive(false);
	        StartCoroutine(TakeDamage(1));
        }
        else if (altcineva.CompareTag("Finish"))
        {
	        Movement.Instance.Reset();
	        Movement.Instance.MoveTo(altcineva.transform.position, false);
	        Movement.Instance.enabled = false;

			altcineva.GetComponent<PickupInfo>()?.InvokeEffect();

	        altcineva.gameObject.SetActive(false);
	        StartCoroutine(FinishLevel());
		}
		else if (altcineva.CompareTag("SlowTime"))
        {
            SlowObstacles(3f);
            altcineva.gameObject.SetActive(false);
        }
    }

	private IEnumerator TakeDamage(int dmg)
	{
		yield return new WaitForSeconds(0.2f);
		healthPoints = healthPoints - dmg;
		if (healthPoints == 0)
		{
			DeathMenu.SetActive(true);
			gameObject.SetActive(false);
		}
	}

	private IEnumerator FinishLevel()
	{
		yield return new WaitForSeconds(0.2f);
		FinishMenu.SetActive(true);

		gameObject.SetActive(false);
		int level = PlayerPrefs.GetInt("PlayerLevel", 0);
		PlayerPrefs.SetInt("PlayerLevel", level + 1);
	}

    private void SlowObstacles(float seconds)
    {
        for (int i = 0; i < gameControl.Obstacles.Length; ++i)
        {
            MovingObstacle movingObstacle = gameControl.Obstacles[i].GetComponent<MovingObstacle>();
            SpikesObstacle spikesObstacle = gameControl.Obstacles[i].GetComponent<SpikesObstacle>();
            SpikesManer spikesManer = gameControl.Obstacles[i].GetComponent<SpikesManer>();

            if (movingObstacle != null)
            {
                movingObstacle.speed = movingObstacle.speed / 2;
            }
            if(spikesObstacle != null)
            {
                spikesObstacle.speed = spikesObstacle.speed / 2;
            }
            if (spikesManer != null)
            {
                spikesManer.speed = spikesManer.speed / 2;
            }
        }
        StartCoroutine(NormalSpeedObstacles(seconds));
    }

    private IEnumerator NormalSpeedObstacles(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        for (int i = 0; i < gameControl.Obstacles.Length; ++i)
        {
            MovingObstacle movingObstacle = gameControl.Obstacles[i].GetComponent<MovingObstacle>();
            SpikesObstacle spikesObstacle = gameControl.Obstacles[i].GetComponent<SpikesObstacle>();
            SpikesManer spikesManer = gameControl.Obstacles[i].GetComponent<SpikesManer>();

            if (movingObstacle != null)
            {
                movingObstacle.speed = movingObstacle.speed * 2;
            }
            if (spikesObstacle != null)
            {
                spikesObstacle.speed = spikesObstacle.speed * 2;
            }
            if (spikesManer != null)
            {
                spikesManer.speed = spikesManer.speed * 2;
            }
        }
    }

    public void MiddleSpawnAnim()
    {
        Instantiate(smallSpawnEffect, transform);
    }

    public void EndSpawnAnim()
    {
        Instantiate(spawnEffect, transform);

        movement.enabled = true;
        boxCollider.enabled = true;
    }
}