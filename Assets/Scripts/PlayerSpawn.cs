﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public GameObject player;
    public bool fall;

    private Animator animator;

    private void Awake()
    {
        animator = player.GetComponent<Animator>();
    }

    private void OnEnable()
	{
		if (Movement.Instance != null)
		{
			Movement.Instance.StopAllCoroutines();
			Movement.Instance.ClearDirectionQueue();
		}
		if (PlayerInteract.Instance != null)
		{
			PlayerInteract.Instance.gameObject.transform.position = transform.position;
			PlayerInteract.Instance.gameObject.SetActive(true);
		}

        animator.SetTrigger("SpawnAnimation");
    }
}
