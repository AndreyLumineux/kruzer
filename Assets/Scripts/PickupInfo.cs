﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupInfo : MonoBehaviour
{
	public GameObject effect;

	public void InvokeEffect()
	{
		Instantiate(effect, transform.position, Quaternion.identity);
	}
}
