﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class Movement : MonoBehaviour
{
    public static Movement Instance { get; private set; }

    [SerializeField] private new Renderer renderer;
    [SerializeField] private float speed = 1;

    private Vector2 currentDirection;
    private Coroutine moveCoroutine;
    private Vector2 touchStartPosition;
    private const float MIN_SWIPE_UNITS = 30f;
    
    private readonly Queue<Vector2> directionQueue = new Queue<Vector2>();
    private const int MAX_QUEUE_SIZE = 3;

    private void TryEnqueue(Vector2 direction)
    {
        if (directionQueue.Count >= MAX_QUEUE_SIZE)
            return;
        
        Vector2 prev = directionQueue.Count > 0 ? directionQueue.Last() : currentDirection;
        if (direction != prev)
            directionQueue.Enqueue(direction);
    }

    private Vector2? TryDequeue()
    {
        if (directionQueue.Count > 0)
            return directionQueue.Dequeue();
        return null;
    }

    public void ClearDirectionQueue()
    {
        directionQueue.Clear();
    }

    private void Awake()
    {
        if (Instance && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        if (!renderer)
            renderer = GetComponent<Renderer>();
    }

    private void FixedUpdate()
    {
        Vector2? inputDirection = null;
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
                touchStartPosition = touch.position;
            if (touch.phase == TouchPhase.Ended)
            {
                Vector2 touchDirection = touch.position - touchStartPosition;
                if (touchDirection.magnitude > MIN_SWIPE_UNITS)
                {
                    if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                        inputDirection = (touchDirection.x * Vector2.right).normalized;
                    else
                        inputDirection = (touchDirection.y * Vector2.up).normalized;
                }
            }
        }

        float horizontalAxis = Input.GetAxis("Horizontal");
        if (!Mathf.Approximately(horizontalAxis, 0))
            inputDirection = (horizontalAxis * Vector2.right).normalized;

        float verticalAxis = Input.GetAxis("Vertical");
        if (!Mathf.Approximately(verticalAxis, 0))
            inputDirection = (verticalAxis * Vector2.up).normalized;
        
        if (inputDirection.HasValue)
            TryEnqueue(inputDirection.Value);

        if (!CanMoveNext())
            return;
        
        Vector2? nextDirection = TryDequeue();
        if (!nextDirection.HasValue)
            return;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, nextDirection.Value, Mathf.Infinity, Physics2D.AllLayers);
        if (!hit)
            return;
        
        currentDirection = nextDirection.Value;
        MoveTo(hit.point);
    }

    private bool CanMoveNext()
    {
        return moveCoroutine == null;
    }

    public void MoveTo(Vector2 point, bool border = true)
	{
		if (moveCoroutine != null)
			return;

        Vector2 destination = point;
		if (border)
		{
			Vector2 displacement;
			Vector2 dir = (point - new Vector2(transform.position.x, transform.position.y)).normalized;
            displacement.x = -dir.x * renderer.bounds.extents.x;
			displacement.y = -dir.y * renderer.bounds.extents.y;
			destination += displacement;
        }
		moveCoroutine = StartCoroutine(EnumeratorMoveTo(destination));
	}

    private IEnumerator EnumeratorMoveTo(Vector2 point)
    {
        Vector3 point3 = point;
        point3.z = transform.position.z;
        Vector2 initial = transform.position;
        float distance = (point3 - transform.position).magnitude;
        float t = 0;
        while (transform.position != point3)
        {
            t += Time.deltaTime;
            transform.position = Vector2.Lerp(initial, point, t / distance * speed);
            yield return null;
        }

        currentDirection = Vector2.zero;
        moveCoroutine = null;
    }

    public void Reset()
    {
        renderer = GetComponent<Renderer>();
		ClearDirectionQueue();
	    if (moveCoroutine != null)
	    {
			StopCoroutine(moveCoroutine);
		    moveCoroutine = null;
		}
    }
}
