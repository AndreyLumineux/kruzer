﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayOnMenu : MonoBehaviour
{
    public void PlayonMenu()
    {
        SceneManager.LoadScene("Main");
    }
}
