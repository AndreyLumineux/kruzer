using System;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Events;
using UnityEngine.Monetization;

public class Ad : MonoBehaviour
    {
        public string PlacementId = "video";

        public float AdInterval = 120;

        public bool ShouldShowAd =>
            Time.realtimeSinceStartup - LastAdTime > AdInterval && (ShowOnce && !Shown || !ShowOnce);

        public bool ShowOnce;

        public static float LastAdTime = 0f;

        public UnityEvent AfterAdFinished;

        public UnityEvent AfterAdCompletedSuccessfully;

        public AnalyticsEventTracker AdStartEvent;

        public AnalyticsEventTracker AdSkippedEvent;

        public AnalyticsEventTracker AdCompleteEvent;

        public bool Shown { get; private set; }

        protected void Awake()
        {
            if (AfterAdFinished == null)
                AfterAdFinished = new UnityEvent();
            if (AfterAdCompletedSuccessfully == null)
                AfterAdCompletedSuccessfully = new UnityEvent();

            AfterAdFinished.AddListener(() => Shown = true);

            if (Mathf.Approximately(LastAdTime, 0))
                LastAdTime = Time.realtimeSinceStartup;

#if UNITY_EDITOR || DEBUG || DEVELOPMENT_BUILD
            AdInterval = float.PositiveInfinity;
#endif
        }

        public void TryShowAd()
        {
            if (ShouldShowAd)
                ShowAd();
            else
                AfterAdFinished.Invoke();
        }

        protected void ShowAd()
        {
            LastAdTime = Time.realtimeSinceStartup;
            if (Monetization.GetPlacementContent(PlacementId) is ShowAdPlacementContent ad)
                ad.Show(new ShowAdCallbacks { startCallback = AdStartCallback, finishCallback = AdResultCallback });
            else
                AfterAdFinished.Invoke();
        }

        protected virtual void AdStartCallback()
        {
            if (AdStartEvent)
                AdStartEvent.TriggerEvent();
        }

        protected virtual void AdResultCallback(ShowResult result) 
        {
            if (result == ShowResult.Finished)
            {
                if (AdCompleteEvent)
                    AdCompleteEvent.TriggerEvent();
                AfterAdCompletedSuccessfully.Invoke();
            }

            if (result == ShowResult.Skipped)
                if (AdSkippedEvent)
                    AdSkippedEvent.TriggerEvent();
            AfterAdFinished.Invoke();
        }
    }
