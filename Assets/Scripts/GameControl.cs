﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public List<GameObject> levels;
    public int pickupsNumber;
    public int minScore;

    [HideInInspector]
    public GameObject[] Obstacles;

    [HideInInspector]
    public GameObject FinishPickup;

    public void Start()
    {
        foreach (GameObject level in levels)
        {
            level.SetActive(false);
        }

        int playerLevel = PlayerPrefs.GetInt("PlayerLevel", 0);

        if (playerLevel >= levels.Count)
            levels[levels.Count - 1].SetActive(true);
        else
            levels[playerLevel].SetActive(true);

        pickupsNumber = GameObject.FindGameObjectsWithTag("Pickup").Length;
        minScore = (int) Mathf.Ceil(pickupsNumber * 80/100f);

        FinishPickup = GameObject.FindGameObjectWithTag("Finish");
		FinishPickup.SetActive(false);

        Obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene("Main");
    }
}
