﻿using UnityEngine;

public class PlayerFillBar : MonoBehaviour
{
    public Transform fullFillImage;

    public Transform fillImage;

    void Start()
    {
        // Default to an empty bar
        var newScale = this.fillImage.localScale;
        newScale.y = 0;
        this.fillImage.localScale = newScale;
    }

    public void SetFillBar(float fillAmount)
    {
        // Make sure value is between 0 and 0.9
        fillAmount = Mathf.Clamp(fillAmount, 0, 0.9f);

        // Scale the fillImage accordingly
        var newScale = this.fillImage.localScale;
        newScale.y = this.fullFillImage.localScale.y * fillAmount;
        this.fillImage.localScale = newScale;
    }
}